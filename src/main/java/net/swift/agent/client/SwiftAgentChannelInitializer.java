package net.swift.agent.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import net.swift.agent.command.SwiftCommand;

public class SwiftAgentChannelInitializer extends ChannelInboundHandlerAdapter {
	ObjectMapper mapper = new ObjectMapper();

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {		
		SwiftCommand obj = mapper.readValue((String) msg, SwiftCommand.class);
		if(obj.getCommandType() == 0)
			System.out.println("[SERVER] - " + (String) obj.getCommandName() + "\r\n");
		else if(obj.getCommandType() == 1)
		{
			StringBuffer output = new StringBuffer();

			System.out.println("[SERVER] - " + (String) obj.getCommandName() + "\r\n");
			Process p;
			try {
				p = Runtime.getRuntime().exec((String) obj.getCommandName().replaceAll("\"", ""));
				p.waitFor();
				BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

				String line = "";
				while ((line = reader.readLine()) != null) {
					output.append(line + "\n");
				}

			} catch (Exception e) {
				System.out.println("EXCEPTION: " + e.getCause() + e.getMessage());
			}
			Channel incommingChannel = ctx.channel();
			incommingChannel.writeAndFlush(output + "\r\n");
		}

	}
	

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		System.out.println("EXCEPTION: " + cause.getCause() + cause.getMessage());
		ctx.close();
	}

	
}
