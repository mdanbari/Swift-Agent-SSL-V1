package net.swift.agent.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.ApplicationProtocolConfig;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import net.swift.agent.constant.Constant;

public class SwiftAgent {

	private Bootstrap bootstrap = new Bootstrap();
	private SocketAddress addr_;
	private Channel channel_;
	private Timer timer_;	
	private static SslContext sslCtx = null;
	private static Boolean SSL_ENABLE;
	private static String IP_SERVER;
	private static Integer PORT_SERVER;
	private static final Logger logger = LoggerFactory.getLogger(SwiftAgent.class);

	public SwiftAgent(String host, int port, Timer timer) {
		this(new InetSocketAddress(host,port),timer);
	}
	
	public SwiftAgent(InetSocketAddress addr, Timer timer) {
		this.addr_ = addr;
		this.timer_ = timer;
		if(SSL_ENABLE)
			setUpSSLContext();
		
		bootstrap
				.group( new NioEventLoopGroup())
				.channel(NioSocketChannel.class)
				.handler(new SwiftAgentInitializer(sslCtx));
		bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
		scheduleConnect(10);

		
	}

	private void setUpSSLContext() {
		try {
			File[] pemks = SSLCertificateHelper.jksKsToPem(Constant.DEFUALT_KEYSTORE_PATH);
			// File pemts = SSLCertificateHelper.jksTsToPem("truststore.jks");
			sslCtx = SslContextBuilder.forClient()
					// .ciphers(getEnabledSSLCiphers())
					.applicationProtocolConfig(ApplicationProtocolConfig.DISABLED).sessionCacheSize(0).sessionTimeout(0)
					.sslProvider(Constant.PROVIDER)
					// .trustManager(pemts)
					.keyManager(pemks[0], pemks[1]).build();

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(Constant.EXCEPTION_SSL,ex);
		} 
	}	
	
	public static void main(String[] args) throws Exception {
		
		
		if(args.length < 3){
			logger.error(Constant.EXCEPTION_ARGS);
			throw new Exception(Constant.EXCEPTION_ARGS);
		}
		
		SSL_ENABLE  = Boolean.valueOf(args[0]);
		IP_SERVER  =  args[1];
		PORT_SERVER = Integer.valueOf(args[2]);
		
		SwiftAgent swiftAgent = new SwiftAgent(IP_SERVER,PORT_SERVER, new Timer());
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		while (true) {
			Thread.sleep(100);
			String msg = in.readLine();
			swiftAgent.sendMessageToServer(msg);
		}

	}

	private void sendMessageToServer(String msg) throws IOException {
		if( channel_ != null && channel_.isActive() ) {
			ByteBuf buf = channel_.alloc().buffer().writeBytes( msg.getBytes() );
			channel_.writeAndFlush( buf );
		} else {
			logger.error(Constant.EXCEPTION_SEND_MESSAGE);
			throw new IOException(Constant.EXCEPTION_SEND_MESSAGE);
		}
		
	}


	private void doConnect() {
		try {
			ChannelFuture f = bootstrap.connect(addr_);
			f.addListener(new ChannelFutureListener() {
				@Override
				public void operationComplete(ChannelFuture future) throws Exception {
					if (!future.isSuccess()) {// if is not successful, reconnect
						future.channel().close();
						bootstrap.connect(addr_).addListener(this);
					} else {// good, the connection is ok
						channel_ = future.channel();
						// add a listener to detect the connection lost
						addCloseDetectListener(channel_);
						connectionEstablished();

					}
				}
				
				

				private void addCloseDetectListener(Channel channel) {
					// if the channel connection is lost, the
					// ChannelFutureListener.operationComplete() will be called
					channel.closeFuture().addListener(new ChannelFutureListener() {
						@Override
						public void operationComplete(ChannelFuture future) throws Exception {
							connectionLost();
							scheduleConnect(5);
						}

					});

				}
			});
		} catch (Exception ex) {
			scheduleConnect(1000);

		}
	}

	private void scheduleConnect(long millis) {
		timer_.schedule(new TimerTask() {
			@Override
			public void run() {
				doConnect();
			}
		}, millis);
	}

	public void connectionLost() {
		logger.error(Constant.EXCEPTION_SERVER_CONNECTION);		
	}

	public void connectionEstablished() {
		logger.error(Constant.PRINT_SERVER_ESTABLISHED);
	}

	static File getAbsoluteFilePathFromClassPath(final String fileNameFromClasspath) {
		File file = null;
		final URL fileUrl = SwiftAgent.class.getClassLoader().getResource(fileNameFromClasspath);
		if (fileUrl != null) {
			try {
				file = new File(URLDecoder.decode(fileUrl.getFile(), "UTF-8"));
			} catch (final UnsupportedEncodingException e) {
				return null;
			}

			if (file.exists() && file.canRead()) {
				return file;
			} else {
				System.out.println(Constant.PRINT_SERVER_ESTABLISHED + file.getAbsolutePath());
				logger.error(Constant.PRINT_SERVER_ESTABLISHED);
			}

		} else {
			System.out.println("Failed to load " + fileNameFromClasspath);
		}
		return null;
	}

}
