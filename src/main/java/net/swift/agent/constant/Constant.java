package net.swift.agent.constant;

import io.netty.handler.ssl.SslProvider;

public interface Constant {
	
	String DEFUALT_IP = "192.168.1.137";
	Integer DEFUALT_PORT = 8000 ;
	SslProvider PROVIDER = SslProvider.JDK;
	String DEFUALT_KEYSTORE_PATH = "swiftkeystore.jks";
	Boolean DEFUALT_SSL_ENABLE = false;
	
	
	String EXCEPTION_ARGS = "Please insert SSL Status, SERVER IP , SERVER PORT ";
	String EXCEPTION_SSL = "SSL EXCEPTION";
	String EXCEPTION_SEND_MESSAGE = "Can't send message to inactive connection";
	String EXCEPTION_SERVER_CONNECTION = "Connection Lost with Server ... ";
	String PRINT_SERVER_ESTABLISHED = "Connection Established ... ";
	String EXCEPTION_FILE = "Cannot read from {}, maybe the file does not exists? ";
	
	
	
	
	
	 

}
